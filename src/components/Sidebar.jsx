import React from "react";
import { Link, NavLink } from "react-router-dom";
import { PATH } from "../config/path";
import "./style.scss";

const Sidebar = () => {
  return (
    <div className="d-flex flex-column">
      <Link to={PATH.databinding}>Data binding</Link>
      <NavLink to={PATH.databindingcondition}>
        Data binding with condition
      </NavLink>
      <NavLink to={PATH.handleevent}>Handle Event</NavLink>
      <NavLink to={PATH.renderwithmap}>Render With Map</NavLink>
      <NavLink to={PATH.btmovie}>BT movie</NavLink>
      <NavLink to={PATH.stylecomponent}>Style Component</NavLink>
      <NavLink to={PATH.demostate}>Demo State</NavLink>
      <NavLink to={PATH.demoprops}>Demo Props</NavLink>
      <NavLink to={PATH.btshoe}>BT Shoe</NavLink>
      <NavLink to={PATH.btphone}>BT Phone</NavLink>
      <NavLink end to={PATH.redux}>
        Demo Redux
      </NavLink>
      <NavLink to={PATH.btphoneredux}>BT Phone Redux</NavLink>
      <NavLink to={PATH.btdatveredux}>BT Đặt vé xem phim</NavLink>
      <NavLink to={PATH.btdatvetoolkit}>BT Đặt vé xem phim toolkit</NavLink>
      <NavLink to={PATH.baiTapForm}>Bt form</NavLink>
    </div>
  );
};

export default Sidebar;
