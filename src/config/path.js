export const PATH = {
  about: "/about",
  databinding: "/databinding",
  databindingcondition: "/databindingcondition",
  handleevent: "/handleevent",
  renderwithmap: "/renderwithmap",
  btmovie: "/btmovie",
  movieDetail: "/movie/:movieId", //params
  stylecomponent: "/stylecomponent",
  demostate: "/demostate",
  demoprops: "/demoprops",
  btshoe: "/btshoe",
  btphone: "/btphone",
  redux: "/redux",
  btphoneredux: "/redux/btphone",
  btdatveredux: "/redux/btdatve",
  btdatvetoolkit: "/redux/toolkit",
  baiTapForm: "/btForm",
};
