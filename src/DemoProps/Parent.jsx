import React from 'react'
import Child from './Child'

// property
// props thay đổi => component render lại
// props => không thể thay đổi giá trị của props ở component nhận props
const Parent = (props) => {
    // console.log('props: ', props)
    let { value, bgColor, fs } = props
    console.log('props: ', props)
    return (
        <div>
            <p className="display-4">Parent {value}</p>
            {/* <p>bgColor : {bgColor}</p>
            <p>fontSize : {fs}</p>
            <Child
                data={[1, 2, 3, 4, 5]}
                sum={() => {
                    console.log('Hello Word')
                }}
            /> */}
        </div>
    )
}

export default Parent
