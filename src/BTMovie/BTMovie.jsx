//rafce
import React from "react";
import movieList from "./data.json";
import {
  useNavigate,
  generatePath,
  useSearchParams,
  useLocation,
} from "react-router-dom";
import { PATH } from "../config/path";
import { useState } from "react";
import queryString from "query-string";
import { generateSearchString } from "../utils/generateSearchString";

const BTMovie = () => {
  const navigate = useNavigate();
  const { pathname } = useLocation();
  // use navigate là hàm chứa tham số là đường dẫn khi hàm này thực thi sẽ dẫn đến đúng
  //đường dẫn đó
  const [searchParams, setSearchParams] = useSearchParams();
  const [value, setValue] = useState();
  const searchQuery = Object.fromEntries(searchParams);
  const findMovieList = () => {
    const data = movieList.filter((item) =>
      item.tenPhim.toLowerCase().includes(searchQuery.movieName.toLowerCase())
    );
    return data;
  };
  const renderMovie = (arrMovie) => {
    return arrMovie.map((movie) => {
      const path = generatePath(PATH.movieDetail, { movieId: movie.maPhim });
      // giúp cấu hình lại path:id
      return (
        <div key={movie.maPhim} className="col-3 mt-3">
          <div className="card">
            <img
              style={{ width: "250px", height: "350px" }}
              src={movie.hinhAnh}
              alt="..."
            />
            <div className="card-body" style={{ overflow: "hidden" }}>
              <p className="font-weight-bold">{movie.tenPhim}</p>
              <p>{movie.moTa.substring(0, 50)}...</p>
              <button
                className="btn btn-outline-success mt-3"
                onClick={() => {
                  navigate(path);
                }}
              >
                Detail
              </button>
            </div>
          </div>
        </div>
      );
    });
  };
  // lấy giá trị user nhập vào input
  const handelChange = (e) => {
    setValue(e.target.value);
  };
  // chuyển đổi object {key:value} => string (key=value)
  // setParams sẽ chuyển đổi tham số thành param có dạng (?param)
  const serializedData = queryString.stringify({ movieName: value });
  // findMovieList()

  return (
    <div className="container">
      <h1>BTMovie</h1>
      <div>
        <input
          className="form-control"
          placeholder="Nhập tên phim"
          defaultValue={searchQuery?.movieName}
          value={value}
          onChange={handelChange}
        />
        <button
          className="btn btn-success mt-3"
          onClick={() => {
            const seacrchString = generateSearchString({
              movieName: value || undefined,
            });
            navigate(pathname + seacrchString);
          }}
        >
          Tìm kiếm
        </button>
      </div>
      <div className="row">
        {/* {movieList.map((movie) => {
                    const path = generatePath(PATH.movieDetail, { movieId: movie.maPhim })
                    return (
                        <div key={movie.maPhim} className="col-3 mt-3">
                            <div className="card">
                                <img
                                    style={{ width: '250px', height: '350px' }}
                                    src={movie.hinhAnh}
                                    alt="..."
                                />
                                <div className="card-body" style={{ overflow: 'hidden' }}>
                                    <p className="font-weight-bold">{movie.tenPhim}</p>
                                    <p>{movie.moTa.substring(0, 50)}...</p>
                                    <button
                                        className="btn btn-outline-success mt-3"
                                        onClick={() => {
                                            navigate(path)
                                        }}
                                    >
                                        Detail
                                    </button>
                                </div>
                            </div>
                        </div>
                    )
                })} */}

        {renderMovie(searchQuery.movieName ? findMovieList() : movieList)}
      </div>
    </div>
  );
};

export default BTMovie;
