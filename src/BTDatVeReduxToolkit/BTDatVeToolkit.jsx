//rafce
import React from 'react'
import data from './data.json'
import ChairList from './ChairList'
import Result from './Result'
import { useDispatch } from 'react-redux'
import { baiTapDatVeActions } from '../storeToolkit/BTDatVe/slice'

const BTDatVeToolkit = () => {
    const dispatch = useDispatch()
    return (
        <div className="container mt-5">
            <h1>BTDatVe Toolkit</h1>
            <div className="row">
                <div className="col-8">
                    <h1 className="display-4">Đặt vé xem phim</h1>
                    <div className="text-center p-3 font-weight-bold display-4 bg-dark text-white mt-3">
                        SCREEN
                    </div>

                    {/* Dánh sách ghế */}
                    <ChairList data={data} />
                </div>
                <div className="col-4">
                    {/* Kết quả đặt vé */}
                    <Result />
                </div>
            </div>

            <button className="btn btn-info mt-5" onClick={()=>{
                dispatch(baiTapDatVeActions.setNumber(99))
            }}>Test Redux Toolkit</button>
        </div>
    )
}

export default BTDatVeToolkit
