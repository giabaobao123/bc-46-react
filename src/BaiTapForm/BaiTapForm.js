import React from "react";
import FormQuanLy from "./FormQuanLy";
import TableForm from "./TableForm";

const BaiTapForm = () => {
  return (
    <div>
      <h1>BtForm</h1>
      <FormQuanLy />
      <TableForm />
    </div>
  );
};

export default BaiTapForm;
