import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { btFormAtion } from "../storeToolkit/BaiTapForm/slice";

const TableForm = () => {
  const { productlist } = useSelector((state) => state.btFormToolkit);
  const dispatch = useDispatch();
  const renderTbody = () => {
    return productlist.map((prdc) => (
      <tr>
        <td>{prdc.id}</td>
        <td>{prdc.name}</td>
        <td>{prdc.image}</td>
        <td>{prdc.price}</td>
        <td>{prdc.desc}</td>
        <td>{prdc.type}</td>
        <td>
          <button
            className="btn btn-danger"
            onClick={() => dispatch(btFormAtion.deleteProcduct(prdc.id))}
          >
            Xóa
          </button>
          <button
            className="ml-3 btn btn-warning"
            onClick={() => dispatch(btFormAtion.updating(prdc.id))}
          >
            Sửa
          </button>
        </td>
      </tr>
    ));
  };
  return (
    <div>
      <table className="table text-center" border={2}>
        <thead className="bg-dark text-warning">
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Image</th>
            <th>Price</th>
            <th>Desc</th>
            <th>Type</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{renderTbody()}</tbody>
      </table>
    </div>
  );
};

export default TableForm;
