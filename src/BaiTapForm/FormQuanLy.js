import React, { useEffect, useState } from "react";
import TableForm from "./TableForm";
import { useDispatch, useSelector } from "react-redux";
import { btFormAtion } from "../storeToolkit/BaiTapForm/slice";

const FormQuanLy = () => {
  const [formData, setFormData] = useState();
  const { productUpdate } = useSelector((state) => state.btFormToolkit);
  const [state, setState] = useState({
    id: 1,
    name: "bảo",
    type: "good",
    price: 23,
    desc: "hello",
    image: "đẹp",
  });
  const dispatch = useDispatch();
  const handelFormData = (event) => {
    setState(productUpdate);
    const { name, value } = event.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };
  return (
    <div>
      <form
        action=""
        onSubmit={(e) => {
          e.preventDefault();
          dispatch(btFormAtion.addProduct(formData));
        }}
      >
        <h2 className="px-2 py-4 bg-dark text-warning">Product infor</h2>
        <div className="form-group row">
          <div className="col-6">
            <p>ID</p>
            <input
              value={state.id}
              type="text"
              placeholder=""
              className="form-control"
              name="id"
              onChange={handelFormData}
            />
          </div>
          <div className="col-6">
            <p>Name</p>
            <input
              value={state.name}
              type="text"
              placeholder=""
              className="form-control"
              name="name"
              onChange={handelFormData}
            />
          </div>
          <div className="col-6">
            <p>Product Type</p>
            <input
              value={state.type}
              type="text"
              placeholder=""
              className="form-control"
              name="type"
              onChange={handelFormData}
            />
          </div>
          <div className="col-6">
            <p>Price</p>
            <input
              value={state.price}
              type="text"
              placeholder=""
              className="form-control"
              name="price"
              onChange={handelFormData}
            />
          </div>
          <div className="col-6">
            <p>Desc</p>
            <input
              value={state.desc}
              type="text"
              placeholder=""
              className="form-control"
              name="desc"
              onChange={handelFormData}
            />
          </div>
          <div className="col-6">
            <p>Image</p>
            <input
              value={state.image}
              type="text"
              placeholder=""
              className="form-control"
              name="image"
              onChange={handelFormData}
            />
          </div>
          <button className="btn btn-info m-3" type="submit">
            Create
          </button>
          <button className="btn btn-success m-3">Update</button>
        </div>
      </form>
    </div>
  );
};

export default FormQuanLy;
