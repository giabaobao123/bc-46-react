import React, { useState } from 'react'
import { flushSync } from 'react-dom'

const DemoState = () => {
    // state dùng để quản lý trạng thái của component
    // Những thành phần nào trên UI thay đổi khi user tương tác => cần đưa vào state

    // Để tạo 1 state trong function component sử dụng hook useState
    // useState => trả về 1 mảng gồm 2 phần tử => phần tử [0] : state, phần tử [1]: hàm để thay đổi state
    // Giá trị khởi tạo đc gán cho state khi gọi hàm useState và truyền vào tham số (initial value)
    // chỉ đc thay đổi giá trị state = cách gọi hàm setState và truyền vào giá trị mới
    // khi gọi hàm setState và thay đổi giá trị state => component sẽ đc render với giá trị state mới => UI thay đổi
    // Trong 1 component => có thể tạo nhiều state

    const state1 = useState()

    const [state, setState] = useState(true)

    const [isShowMessage, setIsShowMessage] = useState()

    const [abc, setAbc] = useState()

    console.log('state: ', state)

    let isLogin = true
    console.log('isLogin: ', isLogin)

    const handleLogout = () => {
        isLogin = false
        console.log('isLogin: ', isLogin)
    }

    const [number, setNumber] = useState(0)

    // batch state update (react 18)
    const onIncreaNumber = () => {
        // setNumber(number + 1) // 0 + 1
        // setNumber(number + 1) // 0 + 1
        // setNumber(number + 1) // 0 + 1
        // setNumber(number + 1) // 0 + 1
        // setNumber(number + 1) // 0 +1
        // setNumber((currentState) => currentState + 1) // 1
        // setNumber((currentState) => currentState + 1) // 2
        // setNumber((currentState) => currentState + 1) // 3
        // setNumber((currentState) => currentState + 1) // 4
        // setNumber((currentState) => currentState + 1) // 5
        // setNumber(number + 1)
        // setFontSize(fontSize + 5)
        flushSync(() => {
            setNumber(number + 1)
        })
        flushSync(() => {
            setFontSize(fontSize + 10)
        })
    }

    const onDecreaNumber = () => {
        setNumber(number - 1)
    }

    const [fontSize, setFontSize] = useState(20)

    const onIncreaFontSize = () => {
        setFontSize(fontSize + 2)
    }

    const onDecreaFontSize = () => {
        setFontSize(fontSize - 2)
    }

    const [srcImage, setSrcImage] = useState('./images/cars/steel-car.jpg')

    const handleChangeCar = (nameCar) => {
        setSrcImage(`./images/cars/${nameCar}-car.jpg`)
    }
    console.log('Render')
    return (
        <div className="container mt-5">
            {/* <h1>DemoState</h1>
            <p>{isLogin ? 'Hello Nguyễn Viết Hải' : 'Đăng nhập'}</p>
            <button className="btn btn-success mt-3" onClick={handleLogout}>
                Đăng xuất
            </button> */}

            <p>{state ? 'Hello Nguyễn Viết Hải' : 'Đăng nhập'}</p>
            <button
                className="btn btn-success mt-3"
                onClick={() => {
                    setState(false)
                }}
            >
                Đăng xuất
            </button>

            {/* BT number */}
            <div className="my-5">
                <p className="display-3">Number : {number} </p>
                <div className="mt-3">
                    <button className="btn btn-success" onClick={onIncreaNumber}>
                        increaNumber
                    </button>
                    <button className="btn btn-danger ml-3" onClick={onDecreaNumber}>
                        decreaNumber
                    </button>
                </div>
            </div>

            {/* BT font size */}
            <div className="my-5">
                <p className="display-3" style={{ fontSize: `${fontSize}px` }}>
                    Hello BC 46
                </p>
                <div className="mt-3">
                    <button className="btn btn-success" onClick={onIncreaFontSize}>
                        increa Font Size
                    </button>
                    <button className="btn btn-danger ml-3" onClick={onDecreaFontSize}>
                        decrea Font Size
                    </button>
                </div>
            </div>

            {/* BT chọn xe */}
            <div className="mt-5">
                <h2>BT chọn xe</h2>
                <div className="row">
                    <div className="col-8">
                        <img className="img-fluid" src={srcImage} alt="..." />
                    </div>
                    <div className="col-4">
                        <div>
                            <button
                                className="btn btn-dark"
                                // onClick={() => {
                                //     setSrcImage('./images/cars/black-car.jpg')
                                // }}
                                onClick={() => handleChangeCar('black')}
                            >
                                BLACK
                            </button>
                        </div>
                        <div className="mt-3">
                            <button
                                className="btn btn-danger"
                                // onClick={() => {
                                //     setSrcImage('./images/cars/red-car.jpg')
                                // }}
                                onClick={() => handleChangeCar('red')}
                            >
                                RED
                            </button>
                        </div>
                        <div className="mt-3">
                            <button
                                className="btn btn-secondary"
                                // onClick={() => {
                                //     setSrcImage('./images/cars/silver-car.jpg')
                                // }}
                                onClick={() => handleChangeCar('silver')}
                            >
                                SILVER
                            </button>
                        </div>
                        <div className="mt-3">
                            <button
                                className="btn"
                                style={{ backgroundColor: 'grey' }}
                                // onClick={() => {
                                //     setSrcImage('./images/cars/steel-car.jpg')
                                // }}
                                onClick={() => handleChangeCar('steel')}
                            >
                                STEEL
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default DemoState
