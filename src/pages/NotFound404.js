import React from "react";
import { useNavigate } from "react-router-dom";

const NotFound404 = () => {
  const navigate = useNavigate();
  return (
    <div className="container text-center">
      <h1>404 not found</h1>
      <div className="text-center mt-5">
        <button className="btn btn-success" onClick={() => navigate("/")}>
          Home
        </button>
      </div>
    </div>
  );
};

export default NotFound404;
