import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  productlist: [],
  productUpdate: {},
};
const btFormSlice = createSlice({
  name: "btForm",
  initialState,
  reducers: {
    addProduct: (state, action) => {
      state.productlist.push(action.payload);
    },
    deleteProcduct(state, action) {
      let index = state.productlist.findIndex(
        (item) => item.id == action.payload
      );
      state.productlist.splice(index, 1);
    },
    updating(state, action) {
      let index = state.productlist.findIndex(
        (item) => item.id == action.payload
      );
      state.productUpdate = state.productlist[index];
    },
  },
});
export const { actions: btFormAtion, reducer: baiTapFormReducer } = btFormSlice;
