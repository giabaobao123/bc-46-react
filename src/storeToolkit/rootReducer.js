import { combineReducers } from "redux";
import demoReduxReducer from "../store/demoRedux/reducer";
import { btPhoneReducer } from "../store/BTphoneRedux/reducer";
import { btDatveReducer } from "../store/BTDatve/reducer";
import { baiTapDatVeReducer } from "./BTDatVe/slice";
import { baiTapFormReducer } from "./BaiTapForm/slice";

export const rootReducer = combineReducers({
  // reducer của store redux cũ
  demoRedux: demoReduxReducer,
  btPhone: btPhoneReducer,
  btDatVe: btDatveReducer,

  // reducer của redux toolkit
  btDatVeToolkit: baiTapDatVeReducer,
  btFormToolkit: baiTapFormReducer,
});
