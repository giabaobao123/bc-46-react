/**
 *
 * @param : object
 * @return  : seacrch string để query URL
 */
import qs from "qs";

export const generateSearchString = (param) => {
  const searchString = qs.stringify(param, {
    addQueryPrefix: true,
  });
  return searchString;
};
// nhận vào object return về  prama có dạng đường dẫn ('key=value') addQueryPrefix : true =>
// có dấu chấm hỏi đằng trước => ('?key=value')
