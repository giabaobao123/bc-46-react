// import logo from './logo.svg'
// import DemoFunctionComponent from './components/DemoFunctionComponent'
// import DemoClassComponent from './components/DemoClassComponent'
import { Route, Routes } from "react-router-dom";
import "./App.css";
import Home from "./BTComponent/Home";
import BTDatVe from "./BTDatVe/BTDatVe";
import BTDatVeToolkit from "./BTDatVeReduxToolkit/BTDatVeToolkit";
import BTMovie from "./BTMovie/BTMovie";
import BTPhoneRedux from "./BTPhoneRedux/BTPhoneRedux";
import BTPhones from "./BTPhones/BTPhones";
import BTShoes from "./BTShoes/BTShoes";
import DataBindingWithCondition from "./DataBindingWithCondition/DataBindingWithCondition";
import DataBinding from "./Databinding/DataBinding";
import DemoProps from "./DemoProps/DemoProps";
import DemoRedux from "./DemoRedux/DemoRedux";
import HandleEvent from "./HandleEvent/HandleEvent";
import RenderWithMap from "./RenderWithMap/RenderWithMap";
import DemoState from "./state/DemoState";
import StyleComponent from "./styleComponent/StyleComponent";

import HomePage from "./pages/HomePage";
import About from "./pages/About";
import { PATH } from "./config/path";
import MainLayout from "./layouts/MainLayout";
import MovieDetail from "./BTMovie/MovieDetail";
import NotFound404 from "./pages/NotFound404";
import BaiTapForm from "./BaiTapForm/BaiTapForm";

// JSX: Javascript XML => cho phép các bạn viết html 1 cách dễ dàng trong js
// binding:
// attributes: viết theo quy tắc camelCase

// Component: thành phần nhỏ giao diện ứng dụng
// 2 loại component: function component (stateLess component), class component (stateFull component) lifecicle

function App() {
  // const name = 'Nguyễn Viết Hải'
  // const sum = (a, b) => a + b
  return (
    <div>
      {/* <DemoFunctionComponent />

            <DemoFunctionComponent></DemoFunctionComponent>

            <DemoClassComponent /> */}

      {/* BT component */}
      {/* <Home /> */}
      <Routes>
        <Route element={<MainLayout />}>
          {/* <Route index element={<HomePage />} />
                    <Route path={PATH.about} element={<About />} /> */}
          <Route index element={<DataBinding />} />
          <Route
            path={PATH.databindingcondition}
            element={<DataBindingWithCondition />}
          />
          <Route path={PATH.handleevent} element={<HandleEvent />} />
          <Route path={PATH.renderwithmap} element={<RenderWithMap />} />

          <Route path={PATH.btmovie} element={<BTMovie />} />
          <Route path={PATH.movieDetail} element={<MovieDetail />} />

          <Route path={PATH.stylecomponent} element={<StyleComponent />} />
          <Route path={PATH.demostate} element={<DemoState />} />
          <Route path={PATH.demoprops} element={<DemoProps />} />
          <Route path={PATH.btshoe} element={<BTShoes />} />
          <Route path={PATH.btphone} element={<BTPhones />} />
          <Route path={PATH.baiTapForm} element={<BaiTapForm />} />
          {/* <Route path="/demoredux" element={<DemoRedux />} /> */}
          {/* <Route path="/btphoneredux" element={<BTPhoneRedux />} /> */}
          {/* <Route path="/btdatve" element={<BTDatVe />} /> */}
          {/* <Route path="/btdatvetoolkit" element={<BTDatVeToolkit />} /> */}

          {/* Nested router */}
          <Route path={PATH.redux}>
            <Route index element={<DemoRedux />} />
            <Route path={PATH.btphoneredux} element={<BTPhoneRedux />} />
            <Route path={PATH.btdatveredux} element={<BTDatVe />} />
            <Route path={PATH.btdatvetoolkit} element={<BTDatVeToolkit />} />
          </Route>
        </Route>
        <Route path="*" element={<NotFound404 />} />
      </Routes>
    </div>
  );
}

export default App;
